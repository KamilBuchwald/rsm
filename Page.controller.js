sap.ui.define(['jquery.sap.global',
		'sap/m/MessageBox',
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/Label',
		'sap/m/Popover',
		'sap/m/List',
		'sap/m/StandardListItem',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		'sap/m/MessageToast',
		'sap/ui/model/odata/ODataModel',
		"sap/ui/export/Spreadsheet",
		'sap/ui/model/Filter'
	],
	function(MessageBox, jQuery, Button, Dialog, Label, Popover, List, StandardListItem, Fragment, Controller, JSONModel, MessageToast,
		ODataModel, Spreadsheet, Filter) {
		"use strict";

		var PageController = Controller.extend("sap.m.sample.PlanningCalendarModifyAppointments.Page", {

			onInit: function() {
				var oModel = new JSONModel();
				oModel.setData({
					startDate: new Date("2018", "7", "30", "8", "0"),
					rooms: [{

							pic: "images/alfa.png",
							name: "Sala Alfa Budynek A",
							role: "Sala wykładowa",
							test: "test ładny",
							price_per_hour: "120 zł",
							reservations: "12",
							hours: "80",
							total: "9600",

							appointments: [{
									start: new Date("2018", "6", "30", "08", "30"),
									end: new Date("2018", "6", "30", "10", "30"),
									title: "Jan Kowalski",
									pic: "sap-icon://accept",
									type: "Type08",
									email: "email"
									
								},

								{
									start: new Date("2018", "6", "30", "14", "0"),
									end: new Date("2018", "6", "30", "16", "0"),
									title: "Marian Nowak",
									info: "Sala",
									type: "Type08",
									pic: "sap-icon://accept",
									email: "email"
									
								},
							{
									start: new Date("2018", "7", "30", "14", "0"),
									end: new Date("2018", "7", "30", "16", "0"),
									title: "Mieczysław Parandowski",
									info: "Sala",
									type: "Type08",
									pic: "sap-icon://sys-help",
									email: "email"
									
								}
							],

							ProductCollection: [{
								"ProductId": "1 szt",
								"Name": "Projektor",
								"ProductPicUrl": "images/rzutnik.jpg"
							}, {
								"ProductId": "1 szt",
								"Name": "Laptop",
								"ProductPicUrl": "images/laptop.jpg"
							}, {
								"ProductId": "1 szt",
								"Name": "Ekran",
								"ProductPicUrl": "images/ekran.jpg"
							},
							 {
								"ProductId": "2 szt",
								"Name": "Kabel HDMI",
								"ProductPicUrl": "images/hdmi.jpg"
							},
							{
								"ProductId": "2 szt",
								"Name": "Listwa zasilająca",
								"ProductPicUrl": "images/listwa.jpg"
							},
							{
								"ProductId": "1 szt",
								"Name": "Komputer stacjonarny",
								"ProductPicUrl": "images/komputer.jpg"
							}]

						}, {
							pic: "images/beta.png",
							name: "Sala Beta Budynek B",
							role: "Sala konferencyjna",
							price_per_hour: "110 zł",
							reservations: "15",
							hours: "60",
							total: "6600",

							appointments: [{
								start: new Date("2018", "7", "30", "15", "0"),
								end: new Date("2018", "7", "30", "18", "0"),
								title: "Marcin Kaczmarek",
								info: "Online meeting",
								type: "Type08",
								pic: "sap-icon://accept",
								email: "email"
							}, {
								start: new Date("2018", "7", "30", "09", "0"),
								end: new Date("2018", "7", "30", "11", "0"),
								title: "Krzysztof Porościk",
									type: "Type08",
									pic: "sap-icon://accept",
									email: "email"
								
							}, {
								start: new Date("2018", "7", "30", "13", "0"),
								end: new Date("2018", "7", "30", "14", "30"),
								title: "Zbigniew Łąkowiak",
								info: "Online meeting",
								pic: "sap-icon://accept",
								type: "Type08",
								email: "email"
							}, {
								start: new Date("2018", "8", "01", "16", "0"),
								end: new Date("2018", "8", "01", "17", "30"),
								title: "Mirosław Król",
								info: "room 1",
								type: "Type08",
								pic: "sap-icon://accept",
								email: "email"
							}, {
								start: new Date("2018", "8", "01", "11", "0"),
								end: new Date("2018", "8", "01", "14", "30"),
								title: "Bogdan Smoleń",
								info: "Online meeting",
								pic: "sap-icon://accept",
								type: "Type08",
								email: "email"
							}],

							ProductCollection: [{
								"ProductId": "1",
								"Name": "Projektor",
								"ProductPicUrl": "images/rzutnik.jpg"
							}, {
								"ProductId": "1",
								"Name": "Laptop",
								"ProductPicUrl": "images/laptop.jpg"
							}]

						}, {
							pic: "images/delta.png",
							name: "Sala Delta Budynek D",
							role: "Sala Dydaktyczna",
							price_per_hour: "140 zł",
							reservations: "18",
							hours: "92",
							total: "12880",
							appointments: [{
								start: new Date("2017", "0", "15", "08", "30"),
								end: new Date("2017", "0", "15", "09", "30"),
								title: "Jan Kowalski",
								type: "Type08",
								email: "email"
							}]

						}, {
							pic: "images/epsilon.png",
							name: "Sala Epsilon Budynek E",
							role: "Audytorium Wysokie",
							price_per_hour: "125 zł",
							reservations: "10",
							hours: "75",
							total: "9375",
							appointments: [{
								start: new Date("2017", "0", "10", "18", "00"),
								end: new Date("2017", "0", "10", "19", "10"),
								title: "Jan Kowalski",
								info: "Online meeting",
								type: "Type08",
								email: "email"
							}]

						}, {
							pic: "images/lambda.png",
							name: "Sala Lambda Budynek L",
							role: "Sala konferencyjna",
							price_per_hour: "100 zł",
							reservations: "23",
							hours: "104",
							total: "10400",
							appointments: [{
								start: new Date("2017", "0", "10", "18", "00"),
								end: new Date("2017", "0", "10", "19", "10"),
								title: "Discussion of the plan",
								info: "Online meeting",
								type: "Type04",
								email: "email"
							}]
						}, {
							pic: "images/gamma.png",
							name: "Sala Gamma Budnek G",
							role: "Aula wysoka",
							price_per_hour: "130 zł",
							reservations: "16",
							hours: "74",
							total: "10360",
							appointments: [{
								start: new Date("2017", "0", "10", "18", "00"),
								end: new Date("2017", "0", "10", "19", "10"),
								title: "Discussion of the plan",
								info: "Online meeting",
								type: "Type04",
								email: "email"
							}],

							ProductCollection: [{
								"ProductId": "1",
								"Name": "Projektor",
								"ProductPicUrl": "images/rzutnik.jpg"
							}, {
								"ProductId": "1",
								"Name": "Laptop",
								"ProductPicUrl": "images/laptop.jpg"
							}]
						}

					],

					ProductCollection: [{
						"ProductId": "1",
						"Name": "Projektor",
						"ProductPicUrl": "images/rzutnik.jpg"
					}, {
						"ProductId": "1",
						"Name": "Laptop",
						"ProductPicUrl": "images/laptop.jpg"
					}, {
						"ProductId": "1",
						"Name": "Komputer stacjonarny",
						"ProductPicUrl": "images/komputer.jpg"
					}, {
						"ProductId": "1",
						"Name": "Ekran",
						"ProductPicUrl": "images/ekran.jpg"
					}, {
						"ProductId": "1",
						"Name": "glosniki",
						"ProductPicUrl": "images/glosniki.jpg"
					}, {
						"ProductId": "2",
						"Name": "Listwa zasilająca",
						"ProductPicUrl": "images/listwa.jpg"
					}, {
						"ProductId": "1",
						"Name": "Tablica klasyczna",
						"ProductPicUrl": "images/tablica.jpg"
					}, {
						"ProductId": "2",
						"Name": "Kabel VGA",
						"ProductPicUrl": "images/vga.jpg"
					}, {
						"ProductId": "2",
						"Name": "Kabel HDMI",
						"ProductPicUrl": "images/hdmi.jpg"
					}]

				});
				this.getView().setModel(oModel);
				

			},

			onMessageDialogPress: function(oEvent) {
				var dialog = new Dialog({
					title: 'Default Message',
					type: 'Message',
					content: new Text({
						text: 'Build enterprise-ready web applications, responsive to all devices and running on the browser of your choice. That´s OpenUI5.'
					}),
					beginButton: new Button({
						text: 'OK',
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});
				
				

				dialog.open();
			},

			handleAppointmentSelect: function(oEvent) {
				var oAppointment = oEvent.getParameter("appointment");

				if (oAppointment) {
					this._handleSingleAppointment(oAppointment);
				} else {
					this._handleGroupAppointments(oEvent);
				}
				
				
			},

			handleOkButtonConfirmation: function() {

				var oFrag = sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;
	
				oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
				oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
				sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
				sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

				oModel = that.getView().getModel();
				sPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "/appointments";
			
				
				oPersonAppointments = oModel.getProperty(sPath);
				

				var start = oPersonAppointments.length;
				var end = start -1;
				
				oPersonAppointments.splice(end, end);
				oModel.setProperty(sPath, oPersonAppointments);


				oNewAppointment = {
					start: oStartDate,
					end: oEndDate,
					title: sTitle,
					info: sInfoResponse,
					type: "Type08",
					pic: "sap-icon://accept"
				};
			
				oPersonAppointments = oModel.getProperty(sPath);
				
				oPersonAppointments.push(oNewAppointment);

				oModel.setProperty(sPath, oPersonAppointments);

				this._oDetailsPopover.close();
				that.oNewAppointmentDialog.addStyleClass("sapUiContentPadding");
				this.getView().addDependent(that.oNewAppointmentDialog);
				MessageToast.show('Wysłano wiadomość potwierdzającą do rezerwującego');
			},

			handleCancelButtonConfirmation: function(oEvent) {
				var oFrag = sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;


					oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
				oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
				sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
				sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

				oModel = that.getView().getModel();
				sPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "/appointments";
			

				oPersonAppointments = oModel.getProperty(sPath);
				

				var start = oPersonAppointments.length;
				var end = start - 1;

				oPersonAppointments.splice(end, end);
				oModel.setProperty(sPath, oPersonAppointments);

				this._oDetailsPopover.close();
				MessageToast.show('Wysłano wiadomość o odrzuceniu rezerwacji do rezerwującego');
			},

			handleAppointmentCreate: function(oEvent) {
				var oFrag = sap.ui.core.Fragment,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oBeginButton;

				this._createDialog();

				oFrag.byId("myFrag", "selectPerson").setSelectedItem(oFrag.byId("myFrag", "selectPerson").getItems()[0]);

				oDateTimePickerStart = oFrag.byId("myFrag", "startDate");
				oDateTimePickerEnd = oFrag.byId("myFrag", "endDate");
				oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				oDateTimePickerStart.setValue("");
				oDateTimePickerEnd.setValue("");
				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);
				this.oNewAppointmentDialog.open();
			},

			handleAppointmentAddWithContext: function(oEvent) {
				var oFrag = sap.ui.core.Fragment,
					currentRow,
					sPersonName,
					oSelect,
					oSelectedItem,
					oSelectedIntervalStart,
					oStartDate,
					oSelectedIntervalEnd,
					oEndDate,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oBeginButton;

				this._createDialog();

				currentRow = oEvent.getParameter("row");
				sPersonName = currentRow.getTitle();
				oSelect = this.oNewAppointmentDialog.getContent()[0].getContent()[1];
				oSelectedItem = oSelect.getItems().filter(function(oItem) {
					return oItem.getText() === sPersonName;
				})[0];
				oSelect.setSelectedItem(oSelectedItem);


				oSelectedIntervalStart = oEvent.getParameter("startDate");
				oStartDate = oFrag.byId("myFrag", "startDate");
				oStartDate.setDateValue(oSelectedIntervalStart);

				oSelectedIntervalEnd = oEvent.getParameter("endDate");
				oEndDate = oFrag.byId("myFrag", "endDate");
				oEndDate.setDateValue(oSelectedIntervalEnd);

				oDateTimePickerStart = oFrag.byId("myFrag", "startDate");
				oDateTimePickerEnd = oFrag.byId("myFrag", "endDate");
				oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);

				this.oNewAppointmentDialog.open();

			},

			_validateDateTimePicker: function(sValue, oDateTimePicker) {
				if (sValue === "") {
					oDateTimePicker.setValueState("Error");
				} else {
					oDateTimePicker.setValueState("None");
				}
			},

			updateButtonEnabledState: function(oDateTimePickerStart, oDateTimePickerEnd, oButton) {
				var bEnabled = oDateTimePickerStart.getValueState() !== "Error" && oDateTimePickerStart.getValue() !== "" && oDateTimePickerEnd.getValue() !==
					"" && oDateTimePickerEnd.getValueState() !== "Error";


				oButton.setEnabled(bEnabled);
			},

			handleDetailsChangeConfirmation: function(oEvent) {
				var oFrag = sap.ui.core.Fragment,
					oDTPStart = oFrag.byId("myPopoverFrag", "startDate"),
					oDTPEnd = oFrag.byId("myPopoverFrag", "endDate"),
					oOKButton = oFrag.byId("myPopoverFrag", "OKButtonConfirmation");


				this._validateDateTimePicker(oEvent.getParameter("value"), oEvent.oSource);
				this.updateButtonEnabledState(oDTPStart, oDTPEnd, oOKButton);

			},

			handleCreateChange: function(oEvent) {
				var oFrag = sap.ui.core.Fragment,
					oDateTimePickerStart = oFrag.byId("myFrag", "startDate"),
					oDateTimePickerEnd = oFrag.byId("myFrag", "endDate"),
					oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				this._validateDateTimePicker(oEvent.getParameter("value"), oEvent.oSource);
				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);
			},

			_createDialog: function() {
				var oFrag = sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;

				if (!that.oNewAppointmentDialog) {

					that.oNewAppointmentDialog = new Dialog({
						title: 'Dokonaj rezerwacji',
						content: [
							sap.ui.xmlfragment("myFrag", "sap.m.sample.PlanningCalendarModifyAppointments.Create", this)
						],
						beginButton: new Button({
							text: 'Zatwierdź',
							enabled: true,
							press: function() {
								oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
								oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
								sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
								sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

								if (oFrag.byId("myFrag", "startDate").getValueState() !== "Error" && oFrag.byId("myFrag", "endDate").getValueState() !==
									"Error") {

									oNewAppointment = {
										start: oStartDate,
										end: oEndDate,
										title: sTitle,
										info: sInfoResponse,
										type: "Type04",
										pic: "sap-icon://sys-help"
									};
									oModel = that.getView().getModel();
									sPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "/appointments";
								

									oPersonAppointments = oModel.getProperty(sPath);

									oPersonAppointments.push(oNewAppointment);

									oModel.setProperty(sPath, oPersonAppointments);
									that.oNewAppointmentDialog.close();
								}
							}
						}),
						endButton: new Button({
							text: 'Zamknij',
							press: function() {
								that.oNewAppointmentDialog.close();
							}
						})
					});

					that.oNewAppointmentDialog.addStyleClass("sapUiContentPadding");
					this.getView().addDependent(that.oNewAppointmentDialog);

				}
			},

			_createDialogInfo: function() {
				var oFrag = sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;

				if (!that.oNewAppointmentDialog) {

					that.oNewAppointmentDialog = new Dialog({
						title: 'Dokonaj rezerwacji',
						content: [
							sap.ui.xmlfragment("myFrag", "sap.m.sample.PlanningCalendarModifyAppointments.Info", this)
						],
						beginButton: new Button({
							text: 'Zatwierdź',
							enabled: false,
							press: function() {
								oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
								oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
								sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
								sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

								if (oFrag.byId("myFrag", "startDate").getValueState() !== "Error" && oFrag.byId("myFrag", "endDate").getValueState() !==
									"Error") {

									oNewAppointment = {
										start: oStartDate,
										end: oEndDate,
										title: sTitle,
										info: sInfoResponse,
										type: "Type06"
									};
									oModel = that.getView().getModel();
									sPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex();
									var iPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex();

									oPersonAppointments = oModel.getProperty(sPath);

									oPersonAppointments.push(oNewAppointment);

									oModel.setProperty(sPath, oPersonAppointments);
									that.oNewAppointmentDialog.close();
								}
							}
						}),
						endButton: new Button({
							text: 'Zamknij',
							press: function() {
								that.oNewAppointmentDialog.close();
							}
						})
					});

					that.oNewAppointmentDialog.addStyleClass("sapUiContentPadding");
					this.getView().addDependent(that.oNewAppointmentInfo);

				}
			},

			gets_item_path: function() {
				var oFrag = sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;

				if (!that.InfoD) {

					that.InfoD = new Dialog({
						title: 'Dokonaj rezerwacji',
						content: [
							sap.ui.xmlfragment("myFrag", "sap.m.sample.PlanningCalendarModifyAppointments.Create", this)
						]
					});

					oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
					oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
					sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
					sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

					oNewAppointment = {
						start: oStartDate,
						end: oEndDate,
						title: sTitle,
						info: sInfoResponse,
						type: "Type04"
					};
					oModel = that.getView().getModel();
					sPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "/appointments";
					var iPath = "/rooms/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "ProductCollection";

					return iPath;
				}

			},

			_handleSingleAppointment: function(oAppointment) {
				var oFrag = sap.ui.core.Fragment,
					oAppBC,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oInfoInput,
					oOKButton;

				if (!this._oDetailsPopover) {
					this._oDetailsPopover = sap.ui.xmlfragment("ReservationInfo", "sap.m.sample.PlanningCalendarModifyAppointments.Confirmation", this);
					this.getView().addDependent(this._oDetailsPopover);
				}

				oAppBC = oAppointment.getBindingContext();

				this._oDetailsPopover.setBindingContext(oAppBC);

				oDateTimePickerStart = oFrag.byId("ReservationInfo", "startDate");
				oDateTimePickerEnd = oFrag.byId("ReservationInfo", "endDate");
				oInfoInput = oFrag.byId("ReservationInfo", "moreInfo");
				oOKButton = oFrag.byId("ReservationInfo", "OKButtonConfirmation");

				oDateTimePickerStart.setDateValue(oAppointment.getStartDate());
				oDateTimePickerEnd.setDateValue(oAppointment.getEndDate());
				oInfoInput.setValue(oAppointment.getText());

				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oOKButton);
				this._oDetailsPopover.openBy(oAppointment);

			},

			onItemClose: function(oEvent) {
				var oItem = oEvent.getSource(),
					oList = oItem.getParent();

				oList.removeItem(oItem);

				MessageToast.show('Item Closed: ' + oEvent.getSource().getTitle());
			},

			onAcceptPress: function(oEvent) {
				var oItem = oEvent.getSource().getParent().getParent(),
				    
					oList = oItem.getParent();
				
				oList.removeItem(oItem);
				
				
				

				MessageToast.show('Wysłano wiadomość potwierdzającą do rezerwującego');

			},

			onRejectPress: function(oEvent) {
				var oItem = oEvent.getSource().getParent().getParent(),
					oList = oItem.getParent();

				oList.removeItem(oItem);

				MessageToast.show('Wysłano wiadomość o odrzuceniu rezerwacji do rezerwującego');

			},
					handleValueHelp : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			this.inputId = oEvent.getSource().getId();
		
			if (!this._valueHelpDialog) {
				this._valueHelpDialog = sap.ui.xmlfragment(
					"sap.m.sample.PlanningCalendarModifyAppointments.Dialog",
					this
				);
				this.getView().addDependent(this._valueHelpDialog);
			}


			this._valueHelpDialog.getBinding("items").filter([new Filter(
				"Name",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);

			this._valueHelpDialog.open(sInputValue);
		},

		_handleValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"Name",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose : function (evt) {
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this.inputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			evt.getSource().getBinding("items").filter([]);
			
			var ico = evt.getParameter("selectedItem").getIcon();
			var sValue = evt.getParameter("selectedItem").getTitle();
		
			
			var oEntry = {
				ProductId: "1",
				Name: sValue,
				ProductPicUrl: ico
	
			};
			

			var oModel = this.getView().getModel();
			var aEntries = oModel.getData().ProductCollection;
			aEntries.unshift(oEntry);
			oModel.setData({
				ProductCollection: aEntries
			});
		},
			handleDelete: function(oEvent) {

			var oItem = oEvent.getParameter("listItem"),
			oList = oEvent.getSource();

			oList.removeItem(oItem);
			
			MessageToast.show('Usunięto element z wyposażenia sali');
		
		},
		createColumnConfig: function() {
			var aCols = [];

			/* 1. Add a simple text column */
			aCols.push({
				label: 'Nazwa Sali',
				type: 'string',
				property: 'name'
			});

			/* 2. Add a concatenated text column */
			aCols.push({
				label: 'Ilość rezerwacji',
				type: 'string',
				property: 'reservations'
				
			});

			/* 3. Add a simple Integer column */
			aCols.push({
				label: 'Cena wynajęcia sali za godzinę',
				type: 'number',
				property: 'price_per_hour'
				
			});

			/* 4. Add a simple Decimal column */
			aCols.push({
				label: 'Ilość wykorzystanych godzin w miesiącu',
				type: 'number',
				property: 'hours'
			});

			/* 5. Add a custom Decimal column */
			aCols.push({
				label: 'Całkowity zysk z wynajmu sali',
				type: 'number',
				property: 'total'
			});

		

			return aCols;
		},
				onExport: function() {
			var aCols, aProducts, oSettings;

			aCols = this.createColumnConfig();
			aProducts = this.getView().getModel().getProperty("/rooms");

			oSettings = {
				workbook: { columns: aCols },
				dataSource: aProducts
			};

			new Spreadsheet(oSettings)
				.build()
				.then( function() {
					MessageToast.show("Zakończono generowanie raportu");
				});
		},
		selectionChanged: function (oEvent) {
			var oPoint = oEvent.getParameter("point");
			MessageToast.show("Sala: " + oPoint.getLabel() + "  zysk miesięczny:  " + oPoint.getValue()  );
		},
		onSelectionChanged: function (oEvent) {
			var oSegment = oEvent.getParameter("segment");
			
			MessageToast.show("Sala: " + oSegment.getLabel() + " została wynajęta w tym miesiącu " + ((oSegment.getValue()) + " razy"));
		}
			

		});

		return PageController;

	});